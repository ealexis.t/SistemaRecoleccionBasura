<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

	Route::get('/home', 'HomeController@index');

	/*
	|-----------------------------------------------------
	| Routes Admin UserContr--------oller
	|------------------------------------------
	*/
	Route::get('/admin/user', 'Admin\UserController@index');
	Route::post('/admin/user/store', 'Admin\UserController@store');
	Route::get('/admin/user/edit/{id}', 'Admin\UserController@edit');
	Route::any('/admin/user/update', 'Admin\UserController@update');
	Route::any('/admin/user/pass/edit/{id}', 'Admin\UserController@passEdit');
	Route::any('/admin/user/pass/update', 'Admin\UserController@passUpdate');
	Route::post('/admin/user/delete', 'Admin\UserController@delete');

	/*
	|-----------------------------------------------------
	| Routes administrador 
	|--------------------------------------------------
	*/

	Route::get('/administrador/configuraciones', 'Administrador\ConfiguracionesController@index');
	Route::get('/administrador/configuraciones/sitios', 'Administrador\ConfiguracionesController@sitios');
	Route::get('/administrador/configuraciones/personal', 'Administrador\ConfiguracionesController@personal');
	Route::get('/administrador/configuraciones/vehiculos', 'Administrador\ConfiguracionesController@vehiculos');

	Route::post('/administrador/configuraciones/tipoEmpleadoStore', 'Administrador\ConfiguracionesController@tipoEmpleadoStore');
	Route::any('/administrador/configuraciones/tipoEmpleadoUpdate', 'Administrador\ConfiguracionesController@tipoEmpleadoUpdate');

	Route::post('/administrador/configuraciones/tipoSitioStore', 'Administrador\ConfiguracionesController@tipoSitioStore');
	Route::any('/administrador/configuraciones/tipoSitioUpdate', 'Administrador\ConfiguracionesController@tipoSitioUpdate');

	Route::post('/administrador/configuraciones/tipoVehiculoStore', 'Administrador\ConfiguracionesController@tipoVehiculoStore');
	Route::any('/administrador/configuraciones/tipoVehiculoUpdate', 'Administrador\ConfiguracionesController@tipoVehiculoUpdate');

	Route::get('/administrador/empleado', 'Administrador\EmpleadoController@index');
	Route::get('/administrador/empleado/create', 'Administrador\EmpleadoController@create');
	Route::post('/administrador/empleado/store', 'Administrador\EmpleadoController@store');

	Route::get('/administrador/sitios', 'Administrador\SitioController@index');
	Route::get('/administrador/sitio/create', 'Administrador\SitioController@create');

	Route::get('/administrador/vehiculos', 'Administrador\VehiculoController@index');
	Route::get('/administrador/vehiculo/create', 'Administrador\VehiculoController@create');

});