<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'rol' => '1',
            'nombre' => 'Admin',
            'aPaterno' => 'Admin',
            'aMaterno' => 'Admin',
        ]);

        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@gmail.com',
            'password' => bcrypt('user'),
            'rol' => '2',
            'nombre' => 'User',
            'aPaterno' => 'User',
            'aMaterno' => 'User',
        ]);

    }
}