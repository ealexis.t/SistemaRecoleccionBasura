<?php
use Illuminate\Database\Seeder;

class TipoDemarcacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoDemarcacion')->insert([
            'tipodemarcacion' => 'Pais',
        ]);

        DB::table('TipoDemarcacion')->insert([
            'tipodemarcacion' => 'Estado',
        ]);

        DB::table('TipoDemarcacion')->insert([
            'tipodemarcacion' => 'Municipio',
        ]);

    }
}