<?php
use Illuminate\Database\Seeder;

class TipoEmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoEmpresa')->insert([
            'tipoempresa' => 'Supervisión'
        ]);

        DB::table('TipoEmpresa')->insert([
            'tipoempresa' => 'Acarreos'
        ]);

        DB::table('TipoEmpresa')->insert([
            'tipoempresa' => 'Limpieza'
        ]);

    }
}