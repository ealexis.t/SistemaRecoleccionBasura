<?php
use Illuminate\Database\Seeder;

class TipoCercoRutaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoCercoRuta')->insert([
            'cercaruta' => 'Cerca',
        ]);

        DB::table('TipoCercoRuta')->insert([
            'cercaruta' => 'Ruta',
        ]);

    }
}