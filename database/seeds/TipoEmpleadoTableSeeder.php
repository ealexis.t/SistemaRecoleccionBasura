<?php
use Illuminate\Database\Seeder;

class TipoEmpleadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoEmpleado')->insert([
            'tipoempleado' => 'Supervisor'
        ]);

        DB::table('TipoEmpleado')->insert([
            'tipoempleado' => 'Checador'
        ]);

        DB::table('TipoEmpleado')->insert([
            'tipoempleado' => 'Operador'
        ]);

        DB::table('TipoEmpleado')->insert([
            'tipoempleado' => 'Otro'
        ]);
    }
}