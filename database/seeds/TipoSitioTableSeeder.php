<?php
use Illuminate\Database\Seeder;

class TipoSitioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoSitio')->insert([
            'tipositio' => 'Estación de Transferencia'
        ]);

        DB::table('TipoSitio')->insert([
            'tipositio' => 'Planta de Selección'
        ]);

        DB::table('TipoSitio')->insert([
            'tipositio' => 'Sitio de Disposición Final'
        ]);
    }
}