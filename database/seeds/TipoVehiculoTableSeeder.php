<?php
use Illuminate\Database\Seeder;

class TipoVehiculoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoVehiculo')->insert([
            'tipovehiculo' => 'Tracto'
        ]);

        DB::table('TipoVehiculo')->insert([
            'tipovehiculo' => 'Caja'
        ]);

        DB::table('TipoVehiculo')->insert([
            'tipovehiculo' => 'Recolector'
        ]);
    }
}