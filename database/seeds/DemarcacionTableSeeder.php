<?php
use Illuminate\Database\Seeder;

class DemarcacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Demarcacion')->insert([
            'demarcacion' => 'México',
            'id_tipodemarcacion' => '1'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Aguascalientes',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Baja California Norte',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Baja California Sur',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Chiapas',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Chihuahua',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Ciudad de México (D.F.)',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Coahuila',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Durango',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Estado de México',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Guanajuato',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Guerrero',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Hidalgo',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Jalisco',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Morelos',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Nayarit',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Nuevo León',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Oaxaca',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Puebla',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Queretaro',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Quintana Roo',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Sinaloa',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Sonora',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Tabasco',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Tamaulipas',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Tlaxcala',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Veracruz',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Yucatán',
            'id_tipodemarcacion' => '2'
        ]);

        DB::table('Demarcacion')->insert([
            'demarcacion' => 'Zacatecas',
            'id_tipodemarcacion' => '2'
        ]);
    }
}