<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TipoTransporte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TipoTransporte', function (Blueprint $table) {
            $table->increments('id_tipotransporte');
            $table->string('tipotransporte');
            $table->integer('id_tipovehiculo')->unsigned();
            $table->foreign('id_tipovehiculo')->references('id_tipovehiculo')->on('TipoVehiculo')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TipoTransporte');
    }
}