<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Empresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Empresas', function (Blueprint $table) {
            $table->increments('id_empresa');
            $table->string('empresa');
            $table->double('latitud');
            $table->double('longitud');
            $table->string('calle');
            $table->string('noext');
            $table->string('noint');
            $table->string('colonia');
            $table->integer('id_municipio')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->integer('id_pais')->unsigned();
            $table->integer('id_tipoempresa')->unsigned();
            $table->foreign('id_municipio')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_estado')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_pais')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_tipoempresa')->references('id_tipoempresa')->on('tipoempresa')->onDelete('cascade');
            $table->string('cp');
            $table->string('telefono');
            $table->string('rfc');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Empresas');
    }
}
