<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmpresaEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EmpresaEmpleado', function (Blueprint $table) {
            $table->increments('id_empresaempleado');
            $table->string('fecha_inicio');
            $table->string('fecha_fin');
            $table->integer('id_empresa')->unsigned();
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_empresa')->references('id_empresa')->on('Empresas')->onDelete('cascade');
            $table->foreign('id_empleado')->references('id_empleado')->on('Empleados')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EmpresaEmpleado');
    }
}
