<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TPSVehiculosEntrada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TPSVehiculosEntrada', function (Blueprint $table) {
            $table->increments('ticket');
            $table->date('fecha_entrada');
            $table->date('fecha_salida');
            $table->time('hora_entrada');
            $table->time('hora_salida');
            $table->float('peso_carga');
            $table->float('peso_tara');
            $table->float('peso_neto');
            $table->float('volumen');
            $table->string('tolva');
            $table->integer('id_tps')->unsigned();
            /*
            $table->integer('id_tracto')->unsigned();
            $table->integer('id_caja')->unsigned();
            */
            $table->integer('id_operador')->unsigned();
            $table->integer('id_supervisor')->unsigned();
            $table->integer('id_motivocancelacionticket')->unsigned();
            $table->boolean('cancelado');
            $table->foreign('id_tps')->references('id_tps')->on('TPS')->onDelete('cascade');
            /*
            $table->foreign('id_tracto')->references('id_tracto')->on('Tractos')->onDelete('cascade');
            $table->foreign('id_caja')->references('id_caja')->on('Cajas')->onDelete('cascade');
            */
            $table->foreign('id_operador')->references('id_empleado')->on('TPSEmpleado')->onDelete('cascade');
            $table->foreign('id_supervisor')->references('id_empleado')->on('TPSEmpleado');
            $table->foreign('id_motivocancelacionticket')->references('id_motivocancelacionticket')->on('MotivoCancelacionTicket')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TPSVehiculosEntrada');
    }
}
