<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TPSEmpleado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TPSEmpleado', function (Blueprint $table) {
            $table->increments('id_tpsempleado');
            $table->string('fecha_alta');
            $table->string('fecha_naja');
            $table->string('hora_entrada');
            $table->string('hora_salida');  
            $table->integer('id_tps')->unsigned();
            $table->integer('id_empleado')->unsigned();
            $table->foreign('id_tps')->references('id_tps')->on('TPS')->onDelete('cascade');
            $table->foreign('id_empleado')->references('id_empleado')->on('Empleados')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TPSEmpleado');
    }
}
