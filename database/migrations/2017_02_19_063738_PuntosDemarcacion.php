<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PuntosDemarcacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DemarcacionPuntos', function (Blueprint $table) {
            $table->increments('id_punto');
            $table->double('latitud');
            $table->double('longitud');
            $table->integer('secuencia');
            $table->integer('id_demarcacion')->unsigned();
            $table->foreign('id_demarcacion')->references('id_demarcacion')->on('Demarcacion')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DemarcacionPuntos');
    }
}
