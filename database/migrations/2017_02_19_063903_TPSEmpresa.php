<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TPSEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TPSEmpresa', function (Blueprint $table) {
            $table->increments('id_tpsempresa');
            $table->string('fecha_inicio');
            $table->string('fecha_fin');
            $table->integer('id_tps')->unsigned();
            $table->integer('id_empresa')->unsigned();
            $table->foreign('id_tps')->references('id_tps')->on('TPS')->onDelete('cascade');
            $table->foreign('id_empresa')->references('id_empresa')->on('Empresas')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TPSEmpresa');
    }
}
