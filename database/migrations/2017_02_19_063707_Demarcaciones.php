<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Demarcaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Demarcacion', function (Blueprint $table) {
            $table->increments('id_demarcacion');
            $table->string('demarcacion');
            $table->integer('id_tipodemarcacion')->unsigned();
            $table->foreign('id_tipodemarcacion')->references('id_tipodemarcacion')->on('TipoDemarcacion')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Demarcacion');
    }
}
