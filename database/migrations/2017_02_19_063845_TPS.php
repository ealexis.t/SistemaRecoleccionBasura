<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TPS extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TPS', function (Blueprint $table) {
            $table->increments('id_tps');
            $table->string('tps');
            $table->double('latitud');
            $table->double('longitud');
            $table->string('calle');
            $table->string('noext');
            $table->string('noint');
            $table->string('colonia');
            $table->integer('id_municipio')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_tipositio')->unsigned();            
            $table->foreign('id_municipio')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_estado')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_usuario')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_tipositio')->references('id_tipositio')->on('tipositio')->onDelete('cascade');
            $table->string('cp');
            $table->string('telefono');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TPS');
    }
}
