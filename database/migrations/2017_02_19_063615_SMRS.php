<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SMRS extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SMRS', function (Blueprint $table) {
            $table->increments('id_smrs');
            $table->string('smrs');
            $table->string('contrato_no');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->tinyInteger('periodo_gracia');
            $table->string('nombre_tabla');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SMRS');
    }
}
