<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CercaRutaPuntos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CercaRutaPuntos', function (Blueprint $table) {
            $table->increments('id_punto');
            $table->double('latitud');
            $table->double('longitud');
            $table->integer('secuencia');
            $table->integer('id_cercaruta')->unsigned();
            $table->integer('id_tps')->unsigned();
            $table->foreign('id_cercaruta')->references('id_cercaruta')->on('Cercaruta')->onDelete('cascade');
            $table->foreign('id_tps')->references('id_tps')->on('TPS')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CercaRutaPuntos');
    }
}
