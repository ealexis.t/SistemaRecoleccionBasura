<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CercaRuta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CercaRuta', function (Blueprint $table) {
            $table->increments('id_cercaruta');
            $table->string('cercaruta');
            $table->integer('id_tps')->unsigned();
            $table->integer('id_tipocercaruta')->unsigned();
            $table->foreign('id_tps')->references('id_tps')->on('TPS')->onDelete('cascade');
            $table->foreign('id_tipocercaruta')->references('id_tipocercaruta')->on('TipoCercaRuta')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CercaRuta');
    }
}