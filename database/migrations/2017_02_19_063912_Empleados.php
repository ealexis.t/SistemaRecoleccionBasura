<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Empleados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Empleados', function (Blueprint $table) {
            $table->increments('id_empleado');
            $table->string('nombre');
            $table->string('appat');
            $table->string('apmat');
            $table->string('calle');
            $table->string('noext');
            $table->string('noint');
            $table->string('colonia');
            $table->integer('id_municipio')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->integer('id_pais')->unsigned();
            $table->integer('id_tipoempleado')->unsigned();
            $table->foreign('id_municipio')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_estado')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_pais')->references('id_demarcacion')->on('demarcacion')->onDelete('cascade');
            $table->foreign('id_tipoempleado')->references('id_tipoempleado')->on('TipoEmpleado')->onDelete('cascade');
            $table->string('cp');
            $table->string('telefono');
            $table->string('celular');
            $table->string('correo');
            $table->string('rfc');
            $table->string('curp');
            $table->string('tipo_sangre');
            $table->string('foto');
            $table->string('referencia_1_nombre');
            $table->string('referencia_1_tel');
            $table->string('referencia_2_nombre');
            $table->string('referencia_2_tel');
            $table->date('fecha_alta');
            $table->date('fecha_baja');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Empleados');
    }
}
