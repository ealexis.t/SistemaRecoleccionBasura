@extends('layouts.app')

@section('content')

<a href="{{ url('/administrador/empleado') }}"><h4>Lista de Empleados</h4></a>
<hr>

    {{ Form::open(array('name' => 'create','url' => 'administrador/empleado/store',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
    <!--Ficha-->
    <div class="modal-content">
        <div class="modal-header">

            <h4 class="modal-title" id="myModalLabel">Registrar Empleado</h4>
        </div>
        <div class="modal-body text-justify">

            <h4>Datos Personales</h4>
            <div class="form-group">
                <label class="col-md-2 control-label">Nombre:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="nombre" type="text" class="form-control " placeholder="Nombre" value="{{ old('nombre') }}">
                        <div class="bg-danger text-center" id="label-nombre">{{$errors->first('nombre')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Paterno:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="appat" type="text" class="form-control " placeholder="Apellido Paterno" value="{{ old('appat') }}">
                        <div class="bg-danger text-center" id="label-appat">{{$errors->first('appat')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Materno:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="apmat" type="text" class="form-control " placeholder="Apellido Materno" value="{{ old('apmat') }}">
                        <div class="bg-danger text-center" id="label-apmat">{{$errors->first('apmat')}}</div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Tipo empleado:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <select class="form-control" name="id_tipoempleado">
                            <option>Seleccionar</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                        <div class="bg-danger text-center" id="label-id_tipoempleado">{{$errors->first('id_tipoempleado')}}</div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Calle:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="calle" type="text" class="form-control " placeholder="Calle" value="{{ old('calle') }}">
                        <div class="bg-danger text-center" id="label-calle">{{$errors->first('calle')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">No ext:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="noext" type="text" class="form-control " placeholder="No ext" value="{{ old('noext') }}">
                        <div class="bg-danger text-center" id="label-noext">{{$errors->first('noext')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">noint:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="noint" type="text" class="form-control " placeholder="No int" value="{{ old('noint') }}">
                        <div class="bg-danger text-center" id="label-noint">{{$errors->first('noint')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">colonia:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="colonia" type="text" class="form-control " placeholder="No int" value="{{ old('colonia') }}">
                        <div class="bg-danger text-center" id="label-colonia">{{$errors->first('colonia')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Municipio:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="id_municipio" type="text" class="form-control " placeholder="Municipio" value="{{ old('id_municipio') }}">
                        <div class="bg-danger text-center" id="label-id_municipio">{{$errors->first('id_municipio')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Estado:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="id_estado" type="text" class="form-control " placeholder="Estado" value="{{ old('id_estado') }}">
                        <div class="bg-danger text-center" id="label-id_estado">{{$errors->first('id_estado')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">País:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="id_pais" type="text" class="form-control " placeholder="País" value="{{ old('id_pais') }}">
                        <div class="bg-danger text-center" id="label-id_pais">{{$errors->first('id_pais')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">CP:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="cp" type="text" class="form-control " placeholder="País" value="{{ old('cp') }}">
                        <div class="bg-danger text-center" id="label-cp">{{$errors->first('cp')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Telefono:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="telefono" type="text" class="form-control " placeholder="Telefono" value="{{ old('telefono') }}">
                        <div class="bg-danger text-center" id="label-telefono">{{$errors->first('telefono')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Celular:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="celular" type="text" class="form-control " placeholder="Celular" value="{{ old('celular') }}">
                        <div class="bg-danger text-center" id="label-celular">{{$errors->first('celular')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Correo:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="correo" type="text" class="form-control " placeholder="Correo" value="{{ old('correo') }}">
                        <div class="bg-danger text-center" id="label-correo">{{$errors->first('correo')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Foto:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="foto" type="text" class="form-control " placeholder="Foto" value="{{ old('foto') }}">
                        <div class="bg-danger text-center" id="label-foto">{{$errors->first('foto')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">RFC:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="rfc" type="text" class="form-control " placeholder="RFC" value="{{ old('rfc') }}">
                        <div class="bg-danger text-center" id="label-rfc">{{$errors->first('rfc')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">CURP:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="curp" type="text" class="form-control " placeholder="CURP" value="{{ old('curp') }}">
                        <div class="bg-danger text-center" id="label-curp">{{$errors->first('curp')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Tipo sangre:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="tipo_sangre" type="text" class="form-control " placeholder="Tipo sangre" value="{{ old('tipo_sangre') }}">
                        <div class="bg-danger text-center" id="label-tipo_sangre">{{$errors->first('tipo_sangre')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Nombre Referencia 1:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="referencia_1_nombre" type="text" class="form-control " placeholder="Nombre Referencia 1" value="{{ old('referencia_1_nombre') }}">
                        <div class="bg-danger text-center" id="label-referencia_1_nombre">{{$errors->first('referencia_1_nombre')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Telefono referencia 1:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="referencia_1_tel" type="text" class="form-control " placeholder="Telefono Referencia 1" value="{{ old('referencia_1_tel') }}">
                        <div class="bg-danger text-center" id="label-referencia_1_tel">{{$errors->first('referencia_1_tel')}}</div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Nombre Referencia 2:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="referencia_2_nombre" type="text" class="form-control " placeholder="Nombre Referencia 2" value="{{ old('referencia_2_nombre') }}">
                        <div class="bg-danger text-center" id="label-referencia_2_nombre">{{$errors->first('referencia_2_nombre')}}</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Telefono referencia 2:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="referencia_2_tel" type="text" class="form-control " placeholder="Telefono Referencia 2" value="{{ old('referencia_2_tel') }}">
                        <div class="bg-danger text-center" id="label-referencia_2_tel">{{$errors->first('referencia_2_tel')}}</div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Fecha alta:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="fecha_alta" type="text" class="form-control " placeholder="Fecha alta" value="{{ old('fecha_alta') }}">
                        <div class="bg-danger text-center" id="label-fecha_alta">{{$errors->first('fecha_alta')}}</div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <a href="{{ url('/home') }}" class="btn btn-default" >Cancelar</a>
            <button type="submit" class="btn green-haze" >Aceptar</button>
        </div>
    </div>
    {{ Form::close() }}

@endsection
