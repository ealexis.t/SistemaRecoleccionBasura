@extends('layouts.app')

@section('content')
<a href="{{ url('/administrador/sitios') }}"><h4>Lista de Sitios</h4></a>
<hr>
    {{ Form::open(array('name' => 'create','url' => 'administrador/sitio/store',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
    <!--Ficha-->
    <div class="modal-content">
        <div class="modal-header">

            <h4 class="modal-title" id="myModalLabel">Registrar Sitio</h4>
        </div>
        <div class="modal-body text-justify">

            <h4>Datos del Sitio</h4>
            <div class="form-group">
                <label class="col-md-2 control-label">Nombre:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="tps" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Típo de sitio</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <select class="form-control" name="tipo">
                            <option>Seleccionar</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                    </div>
                </div>
            </div>
            <h4>Dirección del Sitio</h4>

            <div class="form-group">
                <label class="col-md-2 control-label">Calle:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="calle" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Num Ext:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="noext" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Num Int:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="noint" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Colonia:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="colonia" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Munucipio:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="municipio" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Estado:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="estado" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">País:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="pais" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Telefono:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="telefono" type="text" class="form-control">
                    </div>
                </div>
            </div>
            
            <h4>Datos de Ubicación</h4>
            
            <div class="form-group">
                <label class="col-md-2 control-label">Longitud:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="longitud" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Latitud:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="latitud" type="text" class="form-control">
                    </div>
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <a href="{{ url('/home') }}" class="btn btn-default" >Cancelar</a>
            <button type="submit" class="btn green-haze" >Aceptar</button>
        </div>
    </div>
    {{ Form::close() }}

@endsection
