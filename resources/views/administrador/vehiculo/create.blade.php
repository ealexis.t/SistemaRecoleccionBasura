@extends('layouts.app')

@section('content')

<a href="{{ url('/administrador/vehiculos') }}"><h4>Lista de Vehiculos</h4></a>
<hr>

    {{ Form::open(array('name' => 'create','url' => 'administrador/vehiculo/store',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
    <!--Ficha-->
    <div class="modal-content">
        <div class="modal-header">

            <h4 class="modal-title" id="myModalLabel">Registrar Vehiculo</h4>
        </div>
        <div class="modal-body text-justify">

            <h4>Datos del Vehículo</h4>

            <div class="form-group">
                <label class="col-md-2 control-label">Tipo Vehículo:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <select class="form-control" id="tipo" onchange="exclusive()">
                            <option value="0">Seleccionar</option>
                            <option value="1">Recolector</option>
                            <option value="2">Tracto</option>
                            <option value="3">Caja</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Empresa:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="id_empresa" type="text" class="form-control" >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Placa:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="placa" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">No Económico:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="noeco" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Marca:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="marca" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Modelo:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="modelo" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Año:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="anio" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Serie Motor:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="seriemotor" type="text" class="form-control">
                    </div>
                </div>
            </div>

            <div id="recolector" class="exclusivo recolector">
            <h4>Caracteristicas del recolector</h4>
                <div class="form-group">
                    <label class="col-md-2 control-label">Caracteristica1:</label>
                    <div class="col-md-10">
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input name="item1" type="text" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Caracteristica2:</label>
                    <div class="col-md-10">
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input name="item1" type="text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div id="tracto" class="exclusivo tracto">
            <h4>Caracteristicas del Tracto</h4>
                <div class="form-group">
                    <label class="col-md-2 control-label">Caracteristica1:</label>
                    <div class="col-md-10">
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input name="item1" type="text" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Caracteristica2:</label>
                    <div class="col-md-10">
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input name="item1" type="text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div id="caja" class="exclusivo caja">
            <h4>Caracteristicas de Caja</h4>
                <div class="form-group">
                    <label class="col-md-2 control-label">Caracteristica1:</label>
                    <div class="col-md-10">
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input name="item1" type="text" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Caracteristica2:</label>
                    <div class="col-md-10">
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input name="item1" type="text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
                    
        </div>
        <div class="modal-footer">
            <a href="{{ url('/home') }}" class="btn btn-default" >Cancelar</a>
            <button type="submit" class="btn green-haze" >Aceptar</button>
        </div>
    </div>
    {{ Form::close() }}

<script type="text/javascript">



function exclusive(){

var tipo = $('#tipo').val();

if(tipo == '0'){
    $('#recolector').hide();
    $('#tracto').hide();
    $('#caja').hide();
}

if(tipo == '1'){
    $('#recolector').show();
    $('#tracto').hide();
    $('#caja').hide();
}

if(tipo == '2'){
    $('#recolector').hide();
    $('#tracto').show();
    $('#caja').hide();
}

if(tipo == '3'){
    $('#recolector').hide();
    $('#tracto').hide();
    $('#caja').show();
}

}
</script>

@endsection
