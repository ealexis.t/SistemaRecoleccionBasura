@extends('layouts.app')

@section('content')
<div class="todo-ui">
    <!-- BEGIN TODO CONTENT -->
    <div class="todo-content">
        <div class="portlet light">
            <!-- PROJECT HEAD -->
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green-sharp hide"></i>
                    <span class="caption-helper">PROJECT:</span> &nbsp; <span class="caption-subject font-green-sharp bold uppercase">Vehiculos</span>

                    <a class="btn default pull-"  href="{{ url('/administrador/vehiculo/create') }}">
                    <i class="fa fa-plus"></i> Agregar Nuevo
                    </a>
                </div>
            </div>
            <!-- end PROJECT HEAD -->
            <div class="portlet-body">
                <table class="table table-striped table-hover table-bordered">
                    <tr>
                        <td>Name Vehiculo 1</td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-info" data-toggle="modal" href="#edit"><i class="fa fa-edit"></i> Editar</a></td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-red-sunglo" data-toggle="modal" href="#delete"><i class="fa fa-trash"></i> Eliminar</a></td>
                    </tr>

                    <tr>
                        <td>Name Vehiculo 2</td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-info" data-toggle="modal" href="#edit"><i class="fa fa-edit"></i> Editar</a></td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-red-sunglo" data-toggle="modal" href="#delete"><i class="fa fa-trash"></i> Eliminar</a></td>
                    </tr>

                    <tr>
                        <td>Name Vehiculo 3</td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-info" data-toggle="modal" href="#edit"><i class="fa fa-edit"></i> Editar</a></td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-red-sunglo" data-toggle="modal" href="#delete"><i class="fa fa-trash"></i> Eliminar</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- END TODO CONTENT -->
</div>


<div class="modal fade" id="edit" tabindex="-1" role="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Editar</h4>
            </div>
            <div class="modal-body">
                 Modal body goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn blue">Confirmar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="delete" tabindex="-1" role="delete" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Eliminar</h4>
            </div>
            <div class="modal-body">
                 Modal body goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn blue">Confirmar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection
