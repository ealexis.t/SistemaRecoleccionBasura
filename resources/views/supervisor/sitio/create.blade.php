@extends('layouts.app')

@section('content')

    {{ Form::open(array('name' => 'create','url' => 'supervisor/empleado/store',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
    <!--Ficha-->
    <div class="modal-content">
        <div class="modal-header">

            <h4 class="modal-title" id="myModalLabel">Registrar Sitio</h4>
        </div>
        <div class="modal-body text-justify">

            <h4>Datos del Sitio</h4>
            <div class="form-group">
                <label class="col-md-2 control-label">Nombre:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="nombre" type="text" class="form-control" >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Tipo de Sitio:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aPaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
           
        </div>
        <div class="modal-footer">
            <a href="{{ url('/home') }}" class="btn btn-default" >Cancelar</a>
            <button type="submit" class="btn green-haze" >Aceptar</button>
        </div>
    </div>
    {{ Form::close() }}

@endsection
