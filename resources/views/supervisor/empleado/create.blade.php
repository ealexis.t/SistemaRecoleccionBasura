@extends('layouts.app')

@section('content')

<a href="{{ url('/supervisor/empleado') }}"><h4>Lista de Empleados</h4></a>
<hr>

    {{ Form::open(array('name' => 'create','url' => 'supervisor/empleado/store',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
    <!--Ficha-->
    <div class="modal-content">
        <div class="modal-header">

            <h4 class="modal-title" id="myModalLabel">Registrar Empleado</h4>
        </div>
        <div class="modal-body text-justify">

            <h4>Datos Personales</h4>
            <div class="form-group">
                <label class="col-md-2 control-label">Nombre:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="nombre" type="text" class="form-control" >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Paterno:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aPaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Materno:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Materno:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Calle:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">noext:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">noint:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">colonia:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Municipio:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Estado:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Pais:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Telefono:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Celular:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Correo:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Foto:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Tipo sangre:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Referencia:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Tel referencia:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Fecha alta:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Tipo empleado:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control">
                    </div>
                </div>
            </div>
         
        </div>
        <div class="modal-footer">
            <a href="{{ url('/home') }}" class="btn btn-default" >Cancelar</a>
            <button type="submit" class="btn green-haze" >Aceptar</button>
        </div>
    </div>
    {{ Form::close() }}

@endsection
