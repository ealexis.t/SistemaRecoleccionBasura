@extends('layouts.app')

@section('content')
<div class="todo-ui">
    <div class="todo-sidebar col-md-3">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption" data-toggle="collapse" data-target=".todo-project-list-content">
                    <span class="caption-subject font-green-sharp bold uppercase">Configuraciones </span>
                   
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <span class="btn green-haze btn-circle btn-sm todo-projects-config" data-close-others="true">
                        <i class="icon-settings"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="portlet-body todo-project-list-content" style="height: auto;">
                <div class="todo-project-list">
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <a href="{{ url('/supervisor/configuraciones/sitios') }}">
                            Sitios </a>
                        </li>
                        <li class="active">
                            <a href="{{ url('/supervisor/configuraciones/personal') }}">
                            Personal</a>
                        </li>
                        <li class="">
                            <a href="{{ url('/supervisor/configuraciones/vehiculos') }}">
                            Vehículos
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END TODO SIDEBAR -->
    <!-- BEGIN TODO CONTENT -->
    <div class="todo-content col-md-9">
        <div class="portlet light">
            <!-- PROJECT HEAD -->
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green-sharp hide"></i>
                    <span class="caption-helper">PROJECT:</span> &nbsp; <span class="caption-subject font-green-sharp bold uppercase">Personal</span>

                    <a class="btn default pull-" data-toggle="modal" href="#create">
                    <i class="fa fa-plus"></i> Agregar Nuevo
                    </a>
                </div>
            </div>
            <!-- end PROJECT HEAD -->
            <div class="portlet-body">
                <table class="table table-striped table-hover table-bordered">
                    <tr>
                        <td>Name Personal 1</td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-info" data-toggle="modal" href="#edit"><i class="fa fa-edit"></i> Editar</a></td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-red-sunglo" data-toggle="modal" href="#delete"><i class="fa fa-trash"></i> Eliminar</a></td>
                    </tr>

                    <tr>
                        <td>Name Personal 2</td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-info" data-toggle="modal" href="#edit"><i class="fa fa-edit"></i> Editar</a></td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-red-sunglo" data-toggle="modal" href="#delete"><i class="fa fa-trash"></i> Eliminar</a></td>
                    </tr>

                    <tr>
                        <td>Name Personal 3</td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-info" data-toggle="modal" href="#edit"><i class="fa fa-edit"></i> Editar</a></td>
                        <td class="text-center"><a class="btn btn-sm btn-circle bg-red-sunglo" data-toggle="modal" href="#delete"><i class="fa fa-trash"></i> Eliminar</a></td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
    <!-- END TODO CONTENT -->
</div>



<div class="modal fade" id="edit" tabindex="-1" role="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Editar</h4>
            </div>
            <div class="modal-body">
                 Modal body goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn blue">Confirmar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="delete" tabindex="-1" role="delete" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Eliminar</h4>
            </div>
            <div class="modal-body">
                 Modal body goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn blue">Confirmar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="create" tabindex="-1" role="create" aria-hidden="true">
    <div class="modal-dialog">
        {{ Form::open(array('name' => 'create','url' => 'supervisor/empleado/store',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Agregar</h4>
            </div>
            <div class="modal-body">
                 
                <h4>Datos del Personal</h4>
                <div class="form-group">
                    <label class="col-md-2 control-label">Nombre:</label>
                    <div class="col-md-10">
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input name="nombre" type="text" class="form-control" >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Tipo de Personal:</label>
                    <div class="col-md-10">
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input name="aPaterno" type="text" class="form-control">
                        </div>
                    </div>
                </div>
           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn blue">Confirmar</button>
            </div>
            
        </div>
        {{ Form::close() }}
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
