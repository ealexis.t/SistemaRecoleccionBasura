@extends('layouts.app')

@section('content')
<div class="todo-ui">
    <div class="todo-sidebar col-md-3">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption" data-toggle="collapse" data-target=".todo-project-list-content">
                    <span class="caption-subject font-green-sharp bold uppercase">Configuraciones </span>
                   
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <span class="btn green-haze btn-circle btn-sm todo-projects-config" data-close-others="true">
                        <i class="icon-settings"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="portlet-body todo-project-list-content" style="height: auto;">
                <div class="todo-project-list">
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <a href="{{ url('/supervisor/configuraciones/sitios') }}">
                            Sitios </a>
                        </li>
                        <li>
                            <a href="{{ url('/supervisor/configuraciones/personal') }}">
                            Personal</a>
                        </li>
                        <li class="a">
                            <a href="{{ url('/supervisor/configuraciones/vehiculos') }}">
                            Vehículos
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END TODO SIDEBAR -->
    <!-- BEGIN TODO CONTENT -->
    <div class="todo-content col-md-9">
        <div class="portlet light">
            <!-- PROJECT HEAD -->
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green-sharp hide"></i>
                    <span class="caption-helper"></span> &nbsp; <span class="caption-subject font-green-sharp bold uppercase"></span>
                </div>
            </div>
            <!-- end PROJECT HEAD -->
            <div class="portlet-body">
               
            </div>
        </div>
    </div>
    <!-- END TODO CONTENT -->
</div>
@endsection
