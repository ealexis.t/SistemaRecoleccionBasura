@extends('layouts.app')

@section('content')
<!-- BEGIN DASHBOARD STATS -->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    Usuarios
                </div>
                <div class="desc">
                </div>
            </div>
            <a class="more" href="{{ url('/admin/user') }}">
            IR <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="{{ url('/administrador/empleado') }}">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    Empleados
                </div>
                <div class="desc">
                </div>
            </div>
            <a class="more" href="{{ url('/administrador/empleado') }}">
            IR <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
        </a>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="{{ url('/supervisor/sitio/create') }}">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    Sitios
                </div>
                <div class="desc">
                </div>
            </div>
            <a class="more" href="{{ url('/supervisor/sitio/create') }}">
            IR <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
        </a>
    </div></div>
@endsection
