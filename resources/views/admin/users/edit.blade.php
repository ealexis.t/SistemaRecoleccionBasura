@extends('layouts.app')

@section('content')

    {{ Form::open(array('name' => 'addUser','url' => '/admin/user/update',  'method' => 'put','class'=>'form-horizontal row-fluid'))}}
    <!--Ficha-->
    <div class="modal-content">
        <div class="modal-header">

            <h4 class="modal-title" id="myModalLabel">Editar Usuario</h4>
        </div>
        <div class="modal-body text-justify">
            <hr>
            <h4>Datos de Acceso</h4>
            <div class="form-group">
                <label class="col-md-2 control-label">Username:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input name="name" type="text" class="form-control" value="{{$user->name}}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Correo:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-envelope-o"></i>
                        <input name="email" type="text" class="form-control" value="{{$user->email}}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Tipo de Usuario:</label>
                <div class="col-md-10">
                    <select name="rol" class="form-control">
                        <option value="">Seleccionar</option>
                        <option value="3" @if($user->rol == 3) selected @endif >Representante</option>
                        <option value="2" @if($user->rol == 2) selected @endif>Auditor</option>
                        <option value="1" @if($user->rol == 1) selected @endif>Administrador</option>
                    </select>
                </div>
            </div>

            <hr>
            <h4>Datos de Personales</h4>
            <div class="form-group">
                <label class="col-md-2 control-label">Nombre:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="nombre" type="text" class="form-control" value="{{$user->nombre}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Paterno:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aPaterno" type="text" class="form-control" value="{{$user->aPaterno}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Materno:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-bell-o"></i>
                        <input name="aMaterno" type="text" class="form-control" value="{{$user->aMaterno}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="id" value="{{$user->id}}">
            <a href="{{ url('/admin/user') }}" class="btn btn-default" >Cancelar</a>
            <button type="submit" class="btn green-haze" >Aceptar</button>
        </div>
    </div>
    {{ Form::close() }}

@endsection
