@extends('layouts.app')

@section('content')
<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/jquery.dataTables.js"></script>
<link rel="stylesheet" href="/assets/css/jquery.dataTables.min.css"/>

<div class="col-md-12">
    <a href="#" class="btn btn-sm bg-green-haze" data-toggle="modal" data-dismiss="modal"  data-target="#addUser">Agregar Usuario</a>
</div>
<br><br>
<table id="foo" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Usuario</th>
            <th>Nombre</th>
            <th>Rol</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->nombre}} {{$user->aPaterno}} {{$user->aMaterno}}</td>
            <td>{{$user->rol}}</td>
            <td><a href="{{ url('/admin/user/pass/edit') }}/{{$user->id}}" class="btn btn-md red-flamingo">Editar Pass</a></td>
            <td><a href="{{ url('/admin/user/edit') }}/{{$user->id}}" class="btn btn-md green-jungle">Editar Usuario</a></td>
            <td>
                {{ Form::open(array('name' => 'deleteUser','url' => '/admin/user/delete',  'method' => 'post'))}}
                <input type="hidden" name="id" value="{{$user->id}}">
                <button type="submit" class="btn btn-md red" >Eliminar</button>
                {{ Form::close() }}

            </td>
        </tr>

        @endforeach
    </tbody>
</table>

<?php
    /*
    |-------------------------------------------------------------
    | Modal Agregar Usuario
    |----------------------------------------------------
    */
 ?>
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        {{ Form::open(array('name' => 'addUser','url' => '/admin/user/store',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
            <!--Ficha-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregra Usuario</h4>
                </div>
                <div class="modal-body text-justify">
                    <hr>
                    <h4>Datos de Acceso</h4>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Username:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-user"></i>
                                <input name="name" type="text" class="form-control" placeholder="Nombre de Usuario">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contraseña:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-lock"></i>
                                <input name="password" type="text" class="form-control" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Correo:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-envelope-o"></i>
                                <input name="email" type="text" class="form-control" placeholder="Correo">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Tipo de Usuario:</label>
                        <div class="col-md-10">
                            <select name="rol" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="3">Representante</option>
                                <option value="2">Auditor</option>
                                <option value="1">Administrador</option>
                            </select>
                        </div>
                    </div>

                    <hr>
                    <h4>Datos de Personales</h4>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Nombre:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-bell-o"></i>
                                <input name="nombre" type="text" class="form-control" placeholder="Nombre">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Apellido Paterno:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-bell-o"></i>
                                <input name="aPaterno" type="text" class="form-control" placeholder="Apellido Paterno">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Apellido Materno:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-bell-o"></i>
                                <input name="aMaterno" type="text" class="form-control" placeholder="Apellido Materno">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn green-haze" >Agregar</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
<script type="text/javascript">
/*
**Ordena alfabéticamente de manera ascendente la tabla usuarios tomando la primera columna como referencia
*/
$("#foo").dataTable({
    order: [[1, "desc"]]
});
</script>
@endsection