@extends('layouts.app')

@section('content')

    {{ Form::open(array('name' => 'passUpdate','url' => '/admin/user/pass/update',  'method' => 'put','class'=>'form-horizontal row-fluid'))}}
    <!--Ficha-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Editar Contraseña</h4>
        </div>
        <div class="modal-body text-justify">
            <hr>
            <h4>Datos de Acceso</h4>
            <div class="form-group">
                <label class="col-md-2 control-label">Username:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input type="text" class="form-control" value="{{$user->name}}" disabled>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Contraseña:</label>
                <div class="col-md-10">
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input name="password" type="text" class="form-control" value="" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="id" value="{{$user->id}}">
            <a href="{{ url('/admin/user') }}" class="btn btn-default" >Cancelar</a>
            <button type="submit" class="btn green-haze" >Aceptar</button>
        </div>
    </div>
    {{ Form::close() }}

@endsection
