<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TipoEmpleado extends Model
{
    //Use table own
    protected $table = 'tipoempleado';
    protected $fillable = [
        'id_tipoempleado',
        'tipoempleado',
    ];
}