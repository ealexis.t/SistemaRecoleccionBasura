

<?php namespace App\Http\Middleware;

use Closure;

class Roles {

	public function handle($request, Closure $next){
		if(!($request->user()->rol == 1)){
		    return redirect('home');
		}

		return $next($request);
	}

}

