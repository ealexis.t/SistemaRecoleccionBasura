<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol == 1){
            return view('admin.dashboard');
        }

        if(Auth::user()->rol == 2){
            return view('administrador.dashboard');
        }

        if(Auth::user()->rol == 3){
            return view('admin.dashboard');
        }

    }
}
