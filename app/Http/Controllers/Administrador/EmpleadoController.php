<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\Empleado;
use Validator;

class EmpleadoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->empleados = DB::table('empleados')
                                ->get();

        view()->share('empleados', $this->empleados);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
            return view('administrador.empleado.index');
    }
    
    public function create()
    {
            return view('administrador.empleado.create');
    }

    public function store(Request $request)
    {
        if(Auth::user()->rol == 1){


             $rules = array(
            'nombre' => 'required',
            'appat' => 'required',
            'apmat' => 'required',
            'calle' => 'required',
            'colonia' => 'required',
            'id_municipio' => 'required',
            'id_estado' => 'required',
            'id_pais' => 'required',
            'id_tipoempleado' => 'required',
            'cp' => 'required',
            'telefono' => 'required',
            'celular' => 'required',
            'correo' => 'required',
            'rfc' => 'required',
            'curp' => 'required',
            'tipo_sangre' => 'required',
            'foto' => 'required',
            'referencia_1_nombre' => 'required',
            'referencia_1_tel' => 'required',
            'referencia_2_nombre' => 'required',
            'referencia_2_tel' => 'required',
            );
            $messages = array(
                'nombre.required' =>'Este campo es requerido',
                'appat.required' =>'Este campo es requerido',
                'apmat.required' =>'Este campo es requerido',
                'calle.required' =>'Este campo es requerido',
                'colonia.required' => 'Este campo es requerido',
                'id_municipio.required' =>'Este campo es requerido',
                'id_estado.required' =>'Este campo es requerido',
                'id_pais.required' =>'Este campo es requerido',
                'id_tipoempleado.required' =>'Este campo es requeridoo',
                'cp.required' =>'Este campo es requerido',
                'telefono.required' =>'Este campo es requerido',
                'celular.required' =>'Este campo es requerido',
                'correo.required' =>'Este campo es requerido',
                'rfc.required' =>'Este campo es requerido',
                'curp.required' =>'Este campo es requerido',
                'tipo_sangre.required' =>'Este campo es requerido',
                'foto.required' =>'Este campo es requerido',
                'referencia_1_nombre.required' =>'Este campo es requerido',
                'referencia_1_tel.required' =>'Este campo es requerido',
                'referencia_2_nombre.required' =>'Este campo es requerido',
                'referencia_2_tel.required' =>'Este campo es requerido',
            );
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput()
                    ->withErrors($validator)
                    ->with('message-error', 'Completar campos requeridos');
            } else {
                $empleado = new Empleado;
                $empleado->nombre = $request->nombre;
                $empleado->appat = $request->appat;
                $empleado->apmat = $request->apmat;
                $empleado->calle = $request->calle;
                $empleado->noext = $request->noext;
                $empleado->noint = $request->noint;
                $empleado->colonia = $request->colonia;
                $empleado->id_municipio = $request->id_municipio;
                $empleado->id_estado = $request->id_estado;
                $empleado->id_pais = $request->id_pais;
                $empleado->id_tipoempleado = $request->id_tipoempleado;
                $empleado->cp = $request->cp;
                $empleado->telefono = $request->telefono;
                $empleado->celular = $request->celular;
                $empleado->correo = $request->correo;
                $empleado->rfc = $request->rfc;
                $empleado->curp = $request->curp;
                $empleado->tipo_sangre = $request->tipo_sangre;
                $empleado->foto = $request->foto;
                $empleado->referencia_1_nombre = $request->referencia_1_nombre;
                $empleado->referencia_1_tel = $request->referencia_1_tel;
                $empleado->referencia_2_nombre = $request->referencia_2_nombre;
                $empleado->referencia_2_tel = $request->referencia_2_tel;
                $empleado->fecha_alta = date("m.d.y");
                $empleado->fecha_baja = date("m.d.y");
                $empleado->save();

                return redirect()->back()->with('message', 'Empleado Agregado');
            }
        }
        else{
            return view('404');
        }
    }


}