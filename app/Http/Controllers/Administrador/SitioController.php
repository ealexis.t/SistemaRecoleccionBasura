<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\User;

class SitioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->users = DB::table('users')
                                ->get();

        view()->share('users', $this->users);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if(Auth::user()->rol == 2){
            return view('administrador.sitio.index');
        }
    }

    public function create()
    {
        if(Auth::user()->rol == 2){
            return view('administrador.sitio.create');
        }
    }


}