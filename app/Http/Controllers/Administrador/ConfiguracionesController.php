<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\TipoEmpleado;
use App\TipoSitio;
use App\TipoVehiculo;
use Validator;

class ConfiguracionesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->users = DB::table('users')
                                ->get();

        view()->share('users', $this->users);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrador.configuraciones.index');
    }

    public function sitios()
    {
        $tipositio = DB::table('tipositio')->get();
        return view('administrador.configuraciones.sitios.index')
                    ->with('tipositio', $tipositio);
    }

    public function personal()
    {
        $tipoempleados = DB::table('tipoempleado')->get();
        return view('administrador.configuraciones.personal.index')
                    ->with('tipoempleados', $tipoempleados);
    }

    public function vehiculos()
    {
        $tipovehiculo = DB::table('tipovehiculo')->get();
        return view('administrador.configuraciones.vehiculos.index')
                    ->with('tipovehiculo', $tipovehiculo);
    }

    /*
    |------------------------------------------------------------
    | Metodos Store
    |----------------------------------------------
    */

    public function tipoEmpleadoStore(Request $request)
    {
        if(Auth::user()->rol == 1){

             $rules = array(
            'tipoempleado' => 'required',
            );
            $messages = array(
                'tipoempleado.required' =>'Este campo es requerido',
            );
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput()
                    ->withErrors($validator)
                    ->with('message-error', 'Completar campos requeridos');
            } else {
                $tipoEmpleado = new TipoEmpleado;
                $tipoEmpleado->tipoempleado = $request->tipoempleado;
                $tipoEmpleado->save();

                return redirect()->back()->with('message', 'Empleado Agregado');
            }
        }
        else{
            return view('404');
        }
    }


    public function tipoSitioStore(Request $request)
    {
        if(Auth::user()->rol == 1){

             $rules = array(
            'tipositio' => 'required',
            );
            $messages = array(
                'tipositio.required' =>'Este campo es requerido',
            );
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput()
                    ->withErrors($validator)
                    ->with('message-error', 'Completar campos requeridos');
            } else {
                $tipoEmpleado = new Tipositio;
                $tipoEmpleado->tipositio = $request->tipositio;
                $tipoEmpleado->save();

                return redirect()->back()->with('message', 'Sitio Agregado');
            }
        }
        else{
            return view('404');
        }
    }


    public function tipoVehiculoStore(Request $request)
    {
        if(Auth::user()->rol == 1){

             $rules = array(
            'tipovehiculo' => 'required',
            );
            $messages = array(
                'tipovehiculo.required' =>'Este campo es requerido',
            );
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput()
                    ->withErrors($validator)
                    ->with('message-error', 'Completar campos requeridos');
            } else {
                $tipovehiculo = new TipoVehiculo;
                $tipovehiculo->tipoVehiculo = $request->tipovehiculo;
                $tipovehiculo->save();

                return redirect()->back()->with('message', 'Sitio Agregado');
            }
        }
        else{
            return view('404');
        }
    }



    /*
    |------------------------------------------------------------
    | Metodos Update
    |----------------------------------------------
    */
    public function tipoEmpleadoUpdate(Request $request)
    {
        DB::table('tipoempleado')
        ->where('id_tipoempleado', $request->input('id_tipoempleado'))
        ->update([
            'tipoempleado' => $request->input('tipoempleado'),
        ]);
    
        return redirect()->back()->with('message', 'Información Actualizada');
    }

    public function tipoSitioUpdate(Request $request)
    {
        DB::table('tipositio')
        ->where('id_tipositio', $request->input('id_tipositio'))
        ->update([
            'tipositio' => $request->input('tipositio'),
        ]);
    
        return redirect()->back()->with('message', 'Información Actualizada');
    }

    public function tipoVehiculoUpdate(Request $request)
    {
        DB::table('tipovehiculo')
        ->where('id_tipovehiculo', $request->input('id_tipovehiculo'))
        ->update([
            'tipovehiculo' => $request->input('tipovehiculo'),
        ]);
    
        return redirect()->back()->with('message', 'Información Actualizada');
    }


}