<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->users = DB::table('users')
                                ->get();

        view()->share('users', $this->users);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol == 1){
            return view('admin.users.index');
        }
    }

    public function edit($id)
    {
        $user = User::find($id);

        if(Auth::user()->rol == 1){
            return view('admin.users.edit')->with('user', $user);
        }
    }

    public function passEdit($id)
    {
        $user = User::find($id);

        if(Auth::user()->rol == 1){
            return view('admin.users.pass')->with('user', $user);
        }
    }

    public function store(Request $request)
    {

        if(Auth::user()->rol == 1){

            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->nombre = $request->nombre;
            $user->aPaterno = $request->aPaterno;
            $user->aMaterno = $request->aMaterno;
            $user->rol = $request->rol;
            $user->save();

            return redirect()->back()->with('message', 'Usuario Agregado');
        }
        else{
            return view('404');
        }
    }

    public function update(Request $request)
    {
        if(Auth::user()->rol == '1'){

            DB::table('users')
            ->where('id', $request->input('id'))
            ->update([
                'name' => $request->name,
                'email' => $request->email,
                'nombre' => $request->nombre,
                'aMaterno' => $request->aMaterno,
                'aPaterno' => $request->aPaterno,
                'rol' => $request->rol,
                ]);

            return redirect()->to('/admin/user')->with('message', 'Información Actualizada');
        }
        else{
            return view('404');
        }
    }

    public function passUpdate(Request $request)
    {
        if(Auth::user()->rol == '1'){

            DB::table('users')
            ->where('id', $request->input('id'))
            ->update([
                'password' => bcrypt($request->password),
                ]);

            return redirect()->to('/admin/user')->with('message', 'Contraseña Actualizada');
        }
        else{
            return view('404');
        }
    }


    public function delete(Request $request)
    {
        if(Auth::user()->rol == '1'){

            DB::table('users')->where('id', $request->input('id'))->delete();
            return redirect()->to('/admin/user')->with('message', 'Usuario Eliminado');

        }
        else{
            return view('404');
        }
    }

}