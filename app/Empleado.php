<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Empleado extends Model
{
    //Use table own
    protected $table = 'empleados';
    protected $fillable = [
        'nombre',
        'appat',
        'apmat',
        'calle',
        'colonia',
        'id_municipio',
        'id_estado',
        'id_pais',
        'id_tipoempleado',
        'cp',
        'telefono',
        'celular',
        'rfc',
        'curp',
        'tipo_sangre',
        'foto',
        'referencia_1_nombre',
        'referencia_1_tel',
        'referencia_2_nombre',
        'referencia_2_tel',
    ];
}