<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TipoSitio extends Model
{
    //Use table own
    protected $table = 'tipositio';
    protected $fillable = [
        'id_tipositio',
        'tipositio',
    ];
}