<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TipoVehiculo extends Model
{
    //Use table own
    protected $table = 'tipovehiculo';
    protected $fillable = [
        'id_tipovehiculo',
        'tipovehiculo',
    ];
}